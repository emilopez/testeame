# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0009_remove_exercise_votes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer',
            name='votes',
        ),
    ]
