# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0008_remove_exam_votes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exercise',
            name='votes',
        ),
    ]
