# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0010_remove_answer_votes'),
    ]

    operations = [
        migrations.RenameField(
            model_name='answer',
            old_name='approved_answers',
            new_name='approved',
        ),
    ]
