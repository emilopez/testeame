# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0006_auto_20160323_1830'),
    ]

    operations = [
        migrations.AddField(
            model_name='exam',
            name='votes',
            field=models.IntegerField(default=0),
        ),
    ]
