# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0005_auto_20160321_1617'),
    ]

    operations = [
        migrations.RenameField(
            model_name='answer',
            old_name='approved_comment',
            new_name='approved_answers',
        ),
    ]
