# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0013_exercise_tags'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='text',
            field=models.TextField(null=True, blank=True),
        ),
    ]
