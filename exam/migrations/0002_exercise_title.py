# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='exercise',
            name='title',
            field=models.CharField(default=datetime.datetime(2016, 3, 17, 16, 16, 8, 840764, tzinfo=utc), max_length=200),
            preserve_default=False,
        ),
    ]
