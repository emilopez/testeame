from django.db import models
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.auth.models import User

# pluginable apps installed
from vote.managers import VotableManager
from star_ratings.models import Rating
from taggit.managers import TaggableManager
# Create your models here.

class Exercise(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    votes = VotableManager()
    ratings = GenericRelation(Rating, related_query_name='difficulty')
    tags = TaggableManager()

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def approved_answers(self):
        return self.answers.filter(approved=True)

    def vote(self, user):
        if self.votes.exists(user):
            self.votes.down(user)
        else:
            self.votes.up(user)

    def __str__(self):
        return self.title

class Answer(models.Model):
    exercise = models.ForeignKey('exam.Exercise', related_name='answers')
    author = models.ForeignKey('auth.User')
    text = models.TextField()
    votes = VotableManager()
    created_date = models.DateTimeField(default=timezone.now)
    approved = models.BooleanField(default=False)

    def approve(self):
        self.approved = True
        self.save()

    def vote(self, user):
        if self.votes.exists(user):
            self.votes.down(user)
        else:
            self.votes.up(user)

    def __str__(self):
        return self.text

class Exam(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey('auth.User')
    published_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    exercises = models.ManyToManyField(Exercise)
    course = models.ForeignKey('exam.Course', related_name='exams', blank=True, null=True)
    votes = VotableManager()

    def vote(self, user):
        if self.votes.exists(user):
            self.votes.down(user)
        else:
            self.votes.up(user)

    def __str__(self):
        return self.title

class Course(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey('auth.User')
    text = models.TextField(blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    university = models.ForeignKey('exam.University', related_name='courses', blank=True, null=True)

    def __str__(self):
        return self.title

class University(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey('auth.User')
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

class Teacher(models.Model):
    user = models.OneToOneField(User)
    courses = models.ManyToManyField(Course)
