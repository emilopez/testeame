from django.conf.urls import include, url
from django.contrib import admin
admin.autodiscover()

from . import views

urlpatterns = [
        url(r'^$', views.exercise_list),
        url(r'^exercise/(?P<pk>[0-9]+)/$', views.exercise_detail),
        url(r'^exercise/new/$', views.exercise_new, name='exercise_new'),
        url(r'^exercise/(?P<pk>[0-9]+)/edit/$', views.exercise_edit, name='exercise_edit'),
        url(r'^exercise/(?P<pk>\d+)/publish/$', views.exercise_publish, name='exercise_publish'),
        url(r'^exercise/(?P<pk>\d+)/answer/$', views.add_answer_to_exercise, name='add_answer_to_exercise'),
        url(r'^exercise/(?P<pk>[0-9]+)/vote/$', views.exercise_vote, name='exercise_vote'),
        url(r'^drafts/exercise/$', views.exercise_draft_list, name='exercise_draft_list'),
        url(r'^exercise/order_by/top$', views.exercise_top_list, name='exercise_top_list'),
        url(r'^exercise/order_by/difficulty$', views.exercise_difficulty_list, name='exercise_difficulty_list'),
        url(r'^exercise/tagged/(?P<tag_name>\D+)/$', views.exercise_tagged_list, name='exercise_tag_list'),
        url(r'^exercise/user/(?P<username>\D+)$', views.exercise_list_by_user, name='exercise_list_by_user'),
        url(r'^exam/$', views.exam_list, name='exam_list'),
        url(r'^exam/(?P<pk>[0-9]+)/$', views.exam_detail),
        url(r'^exam/new/$', views.exam_new, name='exam_new'),
        url(r'^exam/(?P<pk>[0-9]+)/vote/$', views.exam_vote, name='exam_vote'),
        url(r'^exam/user/(?P<username>\D+)$', views.exam_list_by_user, name='exercise_list_by_user'),
        url(r'^answer/(?P<pk>\d+)/approve/$', views.answer_approve, name='answer_approve'),
        url(r'^answer/(?P<pk>\d+)/remove/$', views.answer_remove, name='answer_remove'),
        url(r'^answer/(?P<pk>[0-9]+)/vote/$', views.answer_vote, name='answer_vote'),
        url(r'^ratings/', include('star_ratings.urls', namespace='ratings', app_name='ratings')),
        url(r'^user/new/$', views.add_new_teacher, name='add_new_user'),
        url(r'^user/dashboard/$', views.dashboard, name='dashboard'),
        url(r'^course/user/(?P<username>\D+)$', views.user_course_list, name='user_course_list'),

    ]
