from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password

from .models import Exercise, Answer, Exam, Course
from .forms import ExerciseForm, AnswerForm, ExamForm, TeacherForm

import datetime

# ===========================================================================
#                               Exercise views.
# ===========================================================================

def exercise_list(request):
    exercises = Exercise.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    return render(request, 'exam/exercise_list.html', {'exercises': exercises})

def exercise_detail(request, pk):
    exercise = get_object_or_404(Exercise, pk=pk)
    return render(request, 'exam/exercise_detail.html', {'exercise': exercise})

@login_required
def exercise_new(request):
    if request.method == "POST":
        form = ExerciseForm(request.POST)
        if form.is_valid():
            exercise = form.save(commit=False)
            exercise.author = request.user
            #post.published_date = timezone.now()
            exercise.save()
            form.save_m2m()
            return redirect('exam.views.exercise_detail', pk=exercise.pk)
    else:
        form = ExerciseForm()
    return render(request, 'exam/exercise_edit.html', {'form': form})

@login_required
def exercise_edit(request, pk):
    exercise = get_object_or_404(Exercise, pk=pk)
    if request.user != exercise.author :
        return redirect('exam.views.exercise_detail', pk=exercise.pk)
    if request.method == "POST":
        form = ExerciseForm(request.POST, instance=exercise)
        if form.is_valid():
            exercise = form.save(commit=False)
            exercise.author = request.user
            exercise.save()
            form.save_m2m()
            return redirect('exam.views.exercise_detail', pk=exercise.pk)
    else:
        form = ExerciseForm(instance=exercise)
    return render(request, 'exam/exercise_edit.html', {'form': form})

@login_required
def exercise_draft_list(request):
    exercises = Exercise.objects.filter(published_date__isnull=True) | Exercise.objects.filter(published_date__gte=timezone.now())
    exercises = exercises.order_by('created_date')
    return render(request, 'exam/exercise_draft_list.html', {'exercises': exercises})

@login_required
def exercise_publish(request, pk):
    exercise = get_object_or_404(Exercise, pk=pk)
    exercise.publish()
    return redirect('exam.views.exercise_detail', pk=pk)

@login_required
def exercise_vote(request, pk):
    exercise = get_object_or_404(Exercise, pk=pk)
    exercise.vote(request.user)
    return redirect('exam.views.exercise_detail', pk=pk)

def exercise_top_list(request):
    "show return querysets order by number of votes "
    pass

def exercise_difficulty_list(request):
    "order by difficulty average"
    exercises = Exercise.objects.filter(ratings__isnull=False).order_by('ratings__average')
    return render(request, 'exam/exercise_list.html', {'exercises': exercises, 'active_difficulty':'class="active"'})

def exercise_tagged_list(request, tag_name):
    exercises = Exercise.objects.filter(tags__name__in=[tag_name])
    return render(request, 'exam/exercise_list.html', {'exercises': exercises})

def exercise_list_by_user(request, username):
    '''Show all published exercises created by author'''
    exercises = Exercise.objects.filter(published_date__lte=timezone.now()).filter(author__username=username).order_by('-published_date')
    return render(request, 'exam/exercise_list.html', {'exercises': exercises})
    #Exercise.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')


# ===========================================================================
#                               Answer views.
# ===========================================================================
@login_required
def add_answer_to_exercise(request, pk):
    exercise = get_object_or_404(Exercise, pk=pk)
    if request.method == "POST":
        form = AnswerForm(request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.author = request.user
            answer.exercise = exercise
            answer.save()
            return redirect('exam.views.exercise_detail', pk=exercise.pk)
    else:
        form = AnswerForm()
    return render(request, 'exam/add_answer_to_exercise.html', {'form': form})

@login_required
def answer_approve(request, pk):
    answer = get_object_or_404(Answer, pk=pk)
    if request.user == answer.exercise.author:
        answer.approve()
    return redirect('exam.views.exercise_detail', pk=answer.exercise.pk)

@login_required
def answer_remove(request, pk):
    answer = get_object_or_404(Answer, pk=pk)
    exercise_pk = answer.exercise.pk
    if request.user == answer.exercise.author:
        answer.delete()
    return redirect('exam.views.exercise_detail', pk=exercise_pk)

@login_required
def answer_vote(request, pk):
    answer = get_object_or_404(Answer, pk=pk)
    exercise_pk = answer.exercise.pk
    answer.vote(request.user)
    return redirect('exam.views.exercise_detail', pk=exercise_pk)

# ===========================================================================
#                               Exam views.
# ===========================================================================
def exam_list(request):
    exams = Exam.objects.filter(published_date__lte=timezone.now()-datetime.timedelta(days=1)).order_by('published_date')
    return render(request, 'exam/exam_list.html', {'exams': exams})

def exam_detail(request, pk):
    exam = get_object_or_404(Exam, pk=pk)
    return render(request, 'exam/exam_detail.html', {'exam': exam})

@login_required
def exam_new(request):
    if request.method == "POST":
        form = ExamForm(request.POST)
        if form.is_valid():
            exam = form.save(commit=False)
            exam.author = request.user
            #post.published_date = timezone.now()
            exam.save()
            form.save_m2m()
            return redirect('exam.views.exam_detail', pk=exam.pk)
    else:
        form = ExamForm()
    return render(request, 'exam/exam_edit.html', {'form': form})

@login_required
def exam_vote(request, pk):
    exam = get_object_or_404(Exam, pk=pk)
    exam.vote(request.user)
    return redirect('exam.views.exam_detail', pk=exam.pk)

def exam_list_by_user(request, username):
    '''Show all published exams created by author'''
    exams = Exam.objects.filter(published_date__lte=timezone.now()).filter(author__username=username).order_by('-published_date')
    return render(request, 'exam/exam_list.html', {'exams': exams})

'''
Agregar este link en exam_detail.html
<a class="btn btn-default" href="{% url 'exam_edit' pk=exam.pk %}"><span class="glyphicon glyphicon-pencil"></span></a>

@login_required
def exam_edit(request, pk):
    exam = get_object_or_404(Exam, pk=pk)
    if request.method == "POST":
        form = ExamForm(request.POST, instance=exam)
        if form.is_valid():
            exam = form.save(commit=False)
            exam.author = request.user
            exam.save()
            form.save_m2m()
            return redirect('exam.views.exam_detail', pk=exam.pk)
    else:
        form = ExamForm(instance=exam)
    return render(request, 'exam/exam_edit.html', {'form': form})
'''

# ===========================================================================
#                           User views.
# ===========================================================================

def add_new_teacher(request):
    if request.method == "POST":
        form = TeacherForm(request.POST)
        if form.is_valid():
            teacher = form.save(commit=False)
            teacher.password = make_password(form.cleaned_data['password'])
            teacher.save()
            return redirect('exam.views.exercise_list')
    else:
        form = TeacherForm()
    return render(request, 'registration/sign_up.html', {'form': form})

def dashboard(request):
    pass

def user_course_list(request, username):
    '''List all user's courses'''
    courses = Course.objects.filter(author__username=username)
    return render(request, 'exam/course_list.html', {'courses': courses})
