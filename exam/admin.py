from django.contrib import admin

from .models import Exercise, Answer, Exam, Course, University, Teacher
# Register your models here.
admin.site.register(Exercise)
admin.site.register(Answer)
admin.site.register(Exam)
admin.site.register(Course)
admin.site.register(University)
admin.site.register(Teacher)
