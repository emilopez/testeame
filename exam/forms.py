from django import forms
from django.contrib.auth.models import User

from .models import Exercise, Answer, Exam, Teacher

class ExerciseForm(forms.ModelForm):

    class Meta:
        model = Exercise
        fields = ('title', 'text', 'published_date', 'tags',)

class AnswerForm(forms.ModelForm):

    class Meta:
        model = Answer
        fields = ('text',)

class ExamForm(forms.ModelForm):

    class Meta:
        model = Exam
        fields = ('course', 'title', 'published_date', 'exercises',)

class TeacherForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'first_name', 'last_name', )
