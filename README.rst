Mayεuτik
========

Proyecto de software libre para realizar desafíos (preguntas, problemas, ejercicios de programación, etc)
y permitir publicar respuestas. Además, permite armar y gestionar exámenes que contienen estos desafíos.

Si bien incialmente fue pensado para ejercicios de programación, no está limitado a ellos.

Origen del nombre
'''''''''''''''''

Mayεuτik es una mixtura entre el término mayéutica y su origen griego μαιευτικη.
Su significado literal es "dar a luz", motivo por el que Sócrates lo utilizó debido a que su
ideología se basaba en que el saber era dar a luz un nuevo conocimiento a través del cuestionamiento.

Es una técnica que consiste en interrogar a una persona para hacer que llegue al conocimiento a través
de sus propias conclusiones y no a través de un conocimiento aprendido y una teoría pre-conceptualizada.

Modo de funcionamiento
''''''''''''''''''''''

- Todos los desafíos o exámenes se hacen públicos al día siguiente de su fecha de publicación
- Independientemente de su fecha de publicación (generalmente cuando se realiza la evaluación) los desafíos o exámenes se hacen públicos al superar 3 meses de su creación.


Un usuario registrado podrá:

- Crear un desafío y aprobar o desaprobar las respuestas generadas de otros usuarios

- Clasificar un desafío por:
    - Nivel de dificultad (1 a 5 estrellas)
    - Votarlo como Favorito
    - Agregar una solución al desafío

- Armar un examen
    - Eligiendo los desafíos a incluir (cualquiera público o los propios aún privados)
    - Asociarlo a un Curso del que el usuario es miembro.

- Votar como favorita una respuesta

Tecnología a utilizada
''''''''''''''''''''''

- Python 3
- Django 1.8


Instalación
''''''''''''

- Descargar el repo
- Crear un entorno virtual dentro del directorio descargado

.. parsed-literal::

        virtualenv --python=python3.4 myvenv

- Activar el entorno

.. parsed-literal::

        source myvenv/bin/activate

- Instalar django 1.8

.. parsed-literal::

        pip install django==1.8

- Instalar django-vote (para darle likes)

.. parsed-literal::

        pip install django-vote

- Instalar django-star-ratings (para clasificar nivel de dificultad)

.. parsed-literal::

        pip install django-star-ratings

- Instalar Pygments y django-pygments para colorear sintaxis.

.. parsed-literal::

        pip install Pygments

- django-pygments se instala desde git donde está la última versión, que soporta python 3

.. parsed-literal::

        pip install git+https://github.com/odeoncg/django-pygments

- Instalar djago-taggit

.. parsed-literal::

        pip install django-taggit

Todo list
''''''''''
- En los issues


¿Cómo colaborar?
'''''''''''''''''

- Revisá los issues
- Elegí uno que quieras resolver
- Forkeate el repo
- Resolvé el issue
- Haceme un pull request

Si te sentís levemente agradecido podés hacer el esfuerzo de regalarme una cerveza artesanal o un vino.
emiliano [dot] lopez [at] gmail [dot] com
