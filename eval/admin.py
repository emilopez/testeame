from django.contrib import admin

from .models import Exercise, Answer
# Register your models here.

admin.site.register(Exercise)
admin.site.register(Answer)
