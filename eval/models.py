from django.db import models
from django.utils import timezone

# Create your models here.

class Exercise(models.Model):
    author = models.ForeignKey('auth.User')
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    votes = models.IntegerField(default=0)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.text

class Answer(models.Model):
    exercise = models.ForeignKey('eval.Exercise', related_name='answers')
    author = models.CharField(max_length=200)
    text = models.TextField()
    votes = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text
